<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
use Firebase\JWT\JWT;
$router->get('/', function () use ($router) {
    return $router->app->version();
});
$router->get('/Registers',function(){
    $results = app('db')->select("SELECT *FROM Register");
    return response()->json($results);
});
$router->post('/add_reviews',function(Illuminate\Http\Request $request) {
    $Name = $request->input("Name");
    $Date = strtotime($request->input("Date"));
    $Date = date('Y-m-d H:i:s',$Date);
    

    $Place = $request->input("Place");

    $query = app('db')->insert('INSERT into reviews
                                       (Name, Date , Place) VALUES (?,?,?)',
                                       [$Name,
                                        $Date,
                                        $Place] );
   return "Completed" ;

});
$router->put('/update_reviews',function(Illuminate\Http\Request $request) {
    $ID = $request->input("ID");
    $Name = $request->input("Name");

    $Date = strtotime($request->input("Date"));
    $Date = date('Y-m-d H:i:s',$Date);

    $Place = $request->input("Place");

    $query = app('db')->update('UPDATE reviews
                                        SET Name=?,
                                            Date=?,
                                            Place=?
                                        WHERE
                                            ID=?',

                                       [ $Name,
                                         $Date,
                                         $Place,
                                         $ID ] );
   return "Completed" ;

});
$router->delete('/delete_reviews',function(Illuminate\Http\Request $request) {
    $ID = $request->input("ID");

    $query = app('db')->delete('DELETE FROM reviews
                                    WHERE
                                        ID=?',
                                       [$ID] );
    return "Completed" ;
});
$router->post('/register', function(Illuminate\Http\Request $request) {
    $Email = $request->input("Email");
	$username = $request->input("username");
	$password = app('hash')->make($request->input("password"));
	
	$query = app('db')->insert('INSERT into register
					(Username, Email, Password)
					VALUE (?, ?, ?)',
					[ $username,
                      $Email,
					  $password,] );
	return "Ok";
	
});

$router->post('/login', function(Illuminate\Http\Request $request) {
	
	$username = $request->input("username");
	$password = $request->input("password");
	
	$result = app('db')->select("SELECT UserID, password FROM register WHERE Username=?",
									[$username]);
									
	$loginResult = new stdClass();
	
	if(count ($result) == 0) {
			$loginResult->status = "fail";
			$loginResult->reason = "User is not founded";
	}else {
		if(app('hash')->check($password, $result[0]->password)){
			$loginResult->status = "success";
			
			$payload = [
				'iss' => "cafa_system",
				'sub' => $result[0]->UserID,
				'iat' => time(),
				'exp' => time()+ 30 * 60 * 60,
			
			];
			
			$loginResult->token = JWT::encode($payload, env('APP_KEY'));
		}else {
			$loginResult->status = "fail";
			$loginResult->reason = "Incorrect Password";
			return response()->json($loginResult);
		}
	}
	return response()->json($loginResult);
});